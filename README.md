# Java Basics

## Download & Install:

- [IntelliJ community edition](https://www.jetbrains.com/idea/) 
> You can also use your own IDE (such as Eclipse). The trainers of this course are familiar with IntelliJ, so we advise this IDE.
- [Git](https://git-scm.com)
- [Java JDK](https://www.oracle.com/technetwork/java/javase/downloads/index.html)
> You can always choose the latest version here.


## Setup

1) Start IntelliJ
2) Choose 'Get from VCS'
3) Fill in the following URL: "https://gitlab.com/the-ta-crowd/training/java-basics"
4) Choose a folder to clone the project to (IntelliJ doet automatisch een suggestie).
5) Press 'Clone'
6) If this is your first IntelliJ project:
    - Navigate to 'File' -> 'Project Structure'. 
    - In de 'Project' tab, under 'SDK', select the Java JDK. If IntelliJ does not show the JDK in the dropdown, navigate to the folder yourself.

package day_2.challenges.carapp.carparts;

public class Car {

    private int speed;

    public Engine engine = new Engine();
    public GearBox gearBox = new GearBox(5);

    public Car() {
        speed = 0;
    }

    public int getSpeed() {
        return speed;
    }

    public void printState() {
        String carState = "Kitt is driving at a speed of %d km/h in gear %d. The engine is %s";
        String engineState;
        if (engine.isEngineOn()) {
            engineState = "on";
        } else {
            engineState = "off";
        }

        System.out.println(String.format(carState, speed, gearBox.getCurrentGear(), engineState));
    }
}

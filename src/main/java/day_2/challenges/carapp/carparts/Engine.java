package day_2.challenges.carapp.carparts;

public class Engine {

    private boolean engineOn;

    public boolean isEngineOn() {
        return engineOn;
    }

    public void start() {
        engineOn = true;
        System.out.println("Engine started");
    }
}

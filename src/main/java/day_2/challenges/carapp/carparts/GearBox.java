package day_2.challenges.carapp.carparts;

public class GearBox {

    //gear 0 = neutral
    private int currentGear;
    private int maxGear;

    public GearBox(int maxGear) {
        this.maxGear = maxGear;
        currentGear = 0;
    }


    public int getCurrentGear() {
        return currentGear;
    }
}

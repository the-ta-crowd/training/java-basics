package day_2.challenges.carapp;

import day_2.challenges.carapp.carparts.Car;

import java.util.Scanner;

public class CarApp {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
//        Create new car and give it the name 'Kitt'; The car class itself contains its own objects such as engine and gearbox
        Car kitt = new Car();

        boolean quit = false;
        printInstructions();

        while (!quit) {
            kitt.printState();
            System.out.println("Michael, what do you want to do? ");

            String instruction = scanner.nextLine().toLowerCase();
            String[] instructions = instruction.split("\\W+");

            for (String word : instructions) {
                switch (word) {
                    case "start":
                        kitt.engine.start();
                        break;
                    case "stop":
//                        kitt.engine.stop();
                        break;
                    case "accelerate":
//                        kitt.accelerate();
                        break;
                    case "decelerate":
//                        kitt.decelerate();
                        break;
                    case "up":
//                        kitt.gearBox.shiftUp();
                        break;
                    case "down":
//                        kitt.gearBox.shiftDown();
                        break;
                    case "status":
                        break;
                    case "exit":
                        quit = true;
//                      quit = kitt.exit();
                        break;
                }
            }
        }
    }

    private static void printInstructions() {
        System.out.println("You are Michael Knight. Time to go for a ride with Kitt!");
        System.out.println("You can drive Kitt with the following commands:");
        System.out.println("1. start engine");
        System.out.println("2. stop engine");
        System.out.println("3. accelerate");
        System.out.println("4. decelerate");
        System.out.println("5. change gear up");
        System.out.println("6. change gear down");
        System.out.println("7. show current status");
        System.out.println("8. exit car");
    }

}
